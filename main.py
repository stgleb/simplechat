from peristense import *
from models import *
from gevent import monkey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
monkey.patch_all()

from flask import Flask, render_template, session, request, jsonify
from flask.ext.socketio import SocketIO, emit, join_room, leave_room

application = app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

def get_resource_as_string(name, charset='utf-8'):
    print name
    with app.open_resource(name) as f:
        return f.read().decode(charset)

app.jinja_env.globals['get_resource_as_string'] = get_resource_as_string

# @app.route('/')
# def index():
#     return render_template('index.html')

@app.route('/user_name')
def user_name():
    return jsonify({"username":session['username']})

@app.route('/')
def index():
    if 'username' in session and session['username']:
        return render_template('index.html',userName=session['username'])
    else:
        return render_template('login.html')

def authenticate(session,password, user_name):
    print user_name, password
    import hashlib

    m = hashlib.md5()
    m.update(password)
    password_hash = str(m.hexdigest())

    q = database_session.query(User).filter(User.name == user_name)
    result = database_session.execute(q)

    u = result.fetchone()
    if u is None:
        new_user = User(user_name, password_hash)
        database_session.add(new_user)
        database_session.commit()

        session['username'] = user_name
        return render_template('index.html',userName=user_name)
    else:
        print u
        if u[2] == password_hash:
            session['username'] = user_name
            return render_template('index.html',userName=user_name)
        else:
            return render_template('login.html', error='Incorrect password')


@app.route('/login',methods=['POST'])
def login():
    user_name = str(request.form['name'])
    password = str(request.form['password'])

    return authenticate(session,password, user_name)

@app.route('/logout')
def logout():
    session['username'] = None
    return render_template('login.html')


@app.route('/search')
def search():
    args = dict(request.args)
    search_room = args["rooms"][0]

    print search_room
    result = db_get_all_rooms(search_room)

    print "result " + str(result)
    return jsonify(result)

@app.route('/user_rooms')
def user_rooms():
    user_name = session['username']
    print session['username']
    return jsonify(get_user_rooms(user_name))


@socketio.on('my event', namespace='/test')
def test_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my response',
         {'data': message['data'], 'count': session['receive_count']})


@socketio.on('my broadcast event', namespace='/test')
def test_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my response',
         {'data': message['data'], 'count': session['receive_count']},
         broadcast=True)


@socketio.on('join', namespace='/test')
def join(message):
    join_room(message['room'])
    print 'JOIN ROOOM'
    print 'username ' + session['username']
    print 'room name' + message['room']

    db_join_room(session['username'], message['room'])
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my response',
         {'data': 'In rooms: ' + ', '.join(request.namespace.rooms),
          'count': session['receive_count']})

@socketio.on('create', namespace='/test')
def join(message):
    db_create_room(session['username'],message['room'])

    print 'CREATE ROOM'
    join_room(message['room'])
    session['receive_count'] = session.get('receive_count', 0) + 1

    emit('my response',
         {'data': 'In rooms: ' + ', '.join(request.namespace.rooms),
          'count': session['receive_count']})


@socketio.on('leave', namespace='/test')
def leave(message):
    leave_room(message['room'])
    db_leave_room(session['username'],message['room'])

    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my response',
         {'data': 'In rooms: ' + ', '.join(request.namespace.rooms),
          'count': session['receive_count']})


@socketio.on('my room event', namespace='/test')
def send_room_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my response',
         {'data': message['data'], 'count': session['receive_count'],'room':message['room'],'user':message['user']},
         room=message['room'])
    db_send_mesage(message['data'],message['room'])


@socketio.on('connect', namespace='/test')
def test_connect():
    emit('my response', {'data': 'Connected', 'count': 0})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')


if __name__ == '__main__':
    socketio.run(app)

