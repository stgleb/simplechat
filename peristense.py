
from models import *
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import and_

engine = create_engine('mysql://b1758fe6946058:3c13a5bf@eu-cdbr-west-01.cleardb.com/heroku_02e8c6ce500612f', echo=False)
# engine = create_engine('mysql://root:1234@localhost:3306/simple_chat', echo=False)
Session = sessionmaker(bind=engine)
database_session = Session()

def get_session():
    return database_session


def get_room_by_name(room_name, session):
    print room_name

    q = session.query(Chat).filter(Chat.name == room_name)
    result = session.execute(q)
    c = result.fetchone()
    return c


def get_user_by_name(session, user_name):
    print user_name

    q = session.query(User).filter(User.name == user_name)
    result = session.execute(q)
    u = result.fetchone()
    return u


def db_create_room(user_name,room_name):
   session = get_session()

   c = get_room_by_name(room_name, session)
   u = get_user_by_name(session, user_name)

   if c is None:
        chat = Chat(room_name)
        session.add(chat)

        session.commit()

        c = get_room_by_name(room_name, session)

        print u
        print c

        user_chat = UserChat(u[0],c[0])
        session.add(user_chat)
        session.commit()
   else:
       db_join_room(u[1],c[1]);


def db_leave_room(user_name,room_name):
    session = get_session()

    c = get_room_by_name(room_name, session)
    u = get_user_by_name(session, user_name)

    q = session.query(UserChat).filter(and_(UserChat.user_id == u[0],UserChat.chat_id == c[0]))
    result = session.execute(q)
    lst = result.fetchall()

    for l in lst[0]:
        instance = session.query(UserChat).filter_by(id = l).first()

        if instance is not None:
            session.delete(instance)

    session.commit()

def db_join_room(user_name,room_name):
    session = get_session()

    c = get_room_by_name(room_name, session)
    u = get_user_by_name(session, user_name)

    user_chat = UserChat(u[0], c[0])
    session.add(user_chat)

    session.commit()

def db_send_mesage(message,room_name):
    session = get_session()

    c = get_room_by_name(room_name, session)
    m = Log(message,c[0])

    session.add(m)
    session.commit()


def db_get_all_rooms(pattern):
    session = get_session()

    q = session.query(Chat).filter()
    result = session.execute(q)

    response = {"rooms":[]}

    for c in result.fetchall():
        if pattern in c[1]:
            response['rooms'].append(c[1])

    return response


def get_user_rooms(user_name):
    session = get_session()

    u = get_user_by_name(session,user_name)
    output = {"rooms" : []}

    if u is None:
        return output

    q = session.query(UserChat).filter(UserChat.user_id == u[0])
    result = session.execute(q)

    print result

    for u_c in  result.fetchall():
        chat_id = u_c[2]

        q = session.query(Chat).filter(Chat.id == chat_id)
        chats = session.execute(q)

        for c in chats.fetchall():
            output["rooms"].append(c[1])

    print output
    return output

