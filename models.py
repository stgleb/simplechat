from sqlalchemy import create_engine, Table

from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref

Base = declarative_base()

class User(Base):
        __tablename__ = 'users'

        id = Column(Integer, primary_key=True)
        name = Column(String(30))
        password = Column(String(128))

        def __init__(self, name, password):
                self.name = name
                self.password = password

        def __repr__(self):
                return "<Users(%s)>" % (self.name,self.password)

class Log(Base):
        __tablename__ = 'logs'

        id = Column(Integer, primary_key=True)
        chat_id = Column(Integer)
        message = Column(String(300))

        def __init__(self,message,chat_id):
            self.message = message
            self.chat_id = chat_id

        def __repr__(self):
                return "<Log()>"

class Chat(Base):
        __tablename__ = 'chats'

        id = Column(Integer, primary_key=True)
        name = Column(String(30))

        def __init__(self, name):
                self.name = name

        def __repr__(self):
                return "<Chat(%s)>" % (self.id)

class UserChat(Base):
        __tablename__ = 'user_chat'

        id = Column(Integer,primary_key=id)
        user_id = Column(Integer)
        chat_id = Column(Integer)

        def __init__(self, user_id, chat_id):
                self.user_id = user_id
                self.chat_id = chat_id

        def __repr__(self):
                return "<Chat(%s)>" % (self.id)

