

CREATE TABLE users (
         id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
         name VARCHAR (30),
         password VARCHAR (128)
       );


CREATE TABLE chats (
         id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
         name VARCHAR(64)
       );


CREATE TABLE user_chat (
         id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
         user_id INT ,
         chat_id INT ,
         FOREIGN KEY(user_id) REFERENCES users(id),
         FOREIGN KEY(chat_id) REFERENCES chats(id)
       );


CREATE TABLE logs (
         id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
         message VARCHAR(300),
         chat_id INT ,
         FOREIGN KEY(chat_id) REFERENCES chats(id)
       );
